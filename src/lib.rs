use tyr_core::{Entity, Object, Property, Sentence};

use std::{collections::BTreeMap, fmt};

static VOWELS: &str = "iau";
static CONSONANTS: &str = "fshzmnljv";
static DELIMITERS: &str = "' \n";
static SEPARATORS: &str = ",;:.!?";

#[derive(Debug)]
pub enum WordKind {
    Object,
    Specifier,
    Relation,
}

impl fmt::Display for WordKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use WordKind::*;
        match &self {
            Object => write!(f, "object"),
            Specifier => write!(f, "specifier"),
            Relation => write!(f, "relation"),
        }
    }
}

struct Word {
    kind: WordKind,
    name: String,
}

impl Word {
    fn new(
        name: String,
        prefix: bool,
        suffix: bool,
        line: usize,
        column: usize,
    ) -> Result<Self, TokenizeError> {
        use WordKind::*;
        let kind = match (prefix, suffix) {
            (false, false) => return Err(TokenizeErrorKind::InvalidWord(name).at(line, column)),
            (true, false) => Specifier,
            (false, true) => Relation,
            (true, true) => Object,
        };

        Ok(Self { kind, name })
    }
}

#[derive(Debug)]
pub enum TokenizeErrorKind {
    EmptySentence,
    DelimiterWithoutWord,
    SeparatorAfterDelimiter,
    MissingDelimiter,
    InvalidWord(String),
    InvalidCharacter(char),
}

impl TokenizeErrorKind {
    fn at(self, line: usize, column: usize) -> TokenizeError {
        TokenizeError {
            kind: self,
            line,
            column,
        }
    }
}

#[derive(Debug)]
pub struct TokenizeError {
    kind: TokenizeErrorKind,
    line: usize,
    column: usize,
}

impl fmt::Display for TokenizeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let place = format!("{}:{}", self.line, self.column);

        use TokenizeErrorKind::*;
        match &self.kind {
            EmptySentence => write!(f, "The sentence at {place} does not contain any words!"),
            DelimiterWithoutWord => write!(f, "Delimiter not after word at {place}!"),
            SeparatorAfterDelimiter => write!(f, "Separator after delimiter at {place}!"),
            MissingDelimiter => write!(f, "Missing delimiter after separator at {place}!"),
            InvalidWord(word) => write!(f, "Invalid phonotactics for word '{word}' at {place}!"),
            InvalidCharacter(c) => write!(f, "Character '{c}' at {place} is not allowed!"),
        }
    }
}

fn tokenize(chars: impl Iterator<Item = char>) -> Result<Vec<Vec<Word>>, TokenizeError> {
    let mut result = Vec::new();
    let mut current_sentence = Vec::new();
    let mut current_word = String::new();
    let mut next_word = String::new();

    let mut has_prefix = false;
    let mut contains_vowel = false;
    let mut ended_sentence = false;
    let mut line = 0;
    let mut column = 0;

    for c in chars {
        if ended_sentence {
            if DELIMITERS.contains(c) {
                ended_sentence = false;
                continue;
            }

            return Err(TokenizeErrorKind::MissingDelimiter.at(line, column));
        }
        let separator = SEPARATORS.contains(c);
        if separator || DELIMITERS.contains(c) {
            if current_word.is_empty() {
                return Err(if separator {
                    if current_sentence.is_empty() {
                        TokenizeErrorKind::EmptySentence
                    } else {
                        TokenizeErrorKind::SeparatorAfterDelimiter
                    }
                } else {
                    TokenizeErrorKind::DelimiterWithoutWord
                }
                .at(line, column));
            }
            let has_suffix = !next_word.is_empty();
            current_sentence.push(Word::new(
                [current_word, next_word].join(""),
                has_prefix,
                has_suffix,
                line,
                column,
            )?);
            has_prefix = false;
            contains_vowel = false;
            current_word = String::new();
            next_word = String::new();

            if separator {
                result.push(current_sentence);
                current_sentence = Vec::new();
                ended_sentence = true;
            }
        } else if CONSONANTS.contains(c) {
            if contains_vowel {
                next_word.push(c);
            } else {
                current_word.push(c);
            }
        } else if VOWELS.contains(c) {
            if contains_vowel {
                current_sentence.push(Word::new(current_word, has_prefix, false, line, column)?);
                current_word = next_word;
                next_word = String::new();
            } else {
                contains_vowel = true;
            }
            has_prefix = !current_word.is_empty();
            current_word.push(c);
        } else {
            return Err(TokenizeErrorKind::InvalidCharacter(c).at(line, column));
        }

        if c == '\n' {
            line += 1;
            column = 0;
        } else {
            column += 1;
        }
    }

    if !current_word.is_empty() {
        let has_suffix = !next_word.is_empty();
        current_sentence.push(Word::new(
            [current_word, next_word].join(""),
            has_prefix,
            has_suffix,
            line,
            column,
        )?);
    }

    if !current_sentence.is_empty() {
        result.push(current_sentence);
    }

    Ok(result)
}

#[derive(Debug)]
pub enum GrammarError {
    WordsAfterEnd(Vec<String>),
    Unfinished(usize),
    NonExistentWord(String, WordKind),
}

impl fmt::Display for GrammarError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use GrammarError::*;
        match &self {
            WordsAfterEnd(words) => {
                let mut words = words.into_iter();
                let first = words.next().cloned().unwrap_or(String::new());
                write!(
                    f,
                    "The sentence still has words after the last word: {}",
                    words.fold(first, |current, next| format!("{current}, {next}"))
                )
            }
            Unfinished(level) => write!(f, "Sentence is not finished at level {level}!"),
            NonExistentWord(name, kind) => write!(
                f,
                "The {kind} word '{name}' does not exist in this language!"
            ),
        }
    }
}

pub struct Dictionary<O: Object> {
    objects: BTreeMap<String, O>,
    specifiers: BTreeMap<String, O::Specifier>,
    relations: BTreeMap<String, O::Relation>,
}

impl<O: Object> Dictionary<O> {
    pub fn new(
        objects: BTreeMap<String, O>,
        specifiers: BTreeMap<String, O::Specifier>,
        relations: BTreeMap<String, O::Relation>,
    ) -> Self {
        Self {
            objects,
            specifiers,
            relations,
        }
    }
}

fn parse_sentence<O: Object>(
    words: Vec<Word>,
    dictionary: &Dictionary<O>,
) -> Result<Sentence<O>, GrammarError> {
    let mut levels: Vec<(_, Vec<Property<O>>)> = Vec::new();
    let mut properties = Vec::new();
    let mut words = words.into_iter();
    while let Some(Word { kind, name }) = words.next() {
        use WordKind::*;
        match kind {
            Object => {
                let object = if let Some(value) = dictionary.objects.get(&name) {
                    value.clone()
                } else {
                    return Err(GrammarError::NonExistentWord(name, kind));
                };
                if let Some((relation, mut stack_properties)) = levels.pop() {
                    stack_properties.push(Property::Relative {
                        relation,
                        entity: Entity { properties, object },
                    });
                    properties = stack_properties;
                } else {
                    let names: Vec<_> = words.map(|word| word.name).collect();
                    return if names.is_empty() {
                        Ok(Entity { properties, object })
                    } else {
                        Err(GrammarError::WordsAfterEnd(names))
                    };
                }
            }
            Specifier => {
                if let Some(value) = dictionary.specifiers.get(&name) {
                    properties.push(Property::Absolute(value.clone()))
                } else {
                    return Err(GrammarError::NonExistentWord(name, kind));
                };
            }
            Relation => {
                if let Some(value) = dictionary.relations.get(&name) {
                    levels.push((value.clone(), properties));
                    properties = Vec::new();
                } else {
                    return Err(GrammarError::NonExistentWord(name, kind));
                };
            }
        }
    }

    Err(GrammarError::Unfinished(levels.len()))
}

#[derive(Debug)]
pub enum ParseError {
    Tokenize(TokenizeError),
    Grammar(GrammarError),
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ParseError::*;
        match &self {
            Tokenize(err) => write!(f, "{err}"),
            Grammar(err) => write!(f, "{err}"),
        }
    }
}

impl From<TokenizeError> for ParseError {
    fn from(err: TokenizeError) -> Self {
        ParseError::Tokenize(err)
    }
}

impl From<GrammarError> for ParseError {
    fn from(err: GrammarError) -> Self {
        ParseError::Grammar(err)
    }
}

pub fn parse<O: Object>(
    chars: impl Iterator<Item = char>,
    dictionary: &Dictionary<O>,
) -> Result<Vec<Sentence<O>>, ParseError> {
    Ok(tokenize(chars)?
        .into_iter()
        .map(|sentence| parse_sentence(sentence, dictionary))
        .collect::<Result<_, _>>()?)
}
